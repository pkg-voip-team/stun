Source: stun
Section: net
Priority: optional
Maintainer: Debian VoIP Team <pkg-voip-maintainers@lists.alioth.debian.org>
Uploaders: Mark Purcell <msp@debian.org>
Build-Depends: dpkg-build-api (= 1),
               debhelper-compat (= 12)
Standards-Version: 3.7.2
Homepage: http://sourceforge.net/projects/stun/
Vcs-Git: https://salsa.debian.org/pkg-voip-team/stun.git
Vcs-Browser: https://salsa.debian.org/pkg-voip-team/stun

Package: stun-server
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Recommends: stun-client
Replaces: stun (<< 0.97~dfsg-1)
Breaks: stun (<< 0.97~dfsg-1)
Provides: stun
Description: Server daemon for STUN
 The STUN protocol (Simple Traversal of UDP through NATs) is described in the
 IETF RFC 3489, available at http://www.ietf.org/rfc/rfc3489.txt.  It's used to
 help clients behind NAT to tunnel incoming calls through. This server is the
 counterpart to help the client identify the NAT and have it open the proper
 ports for it.
 .
 This package installs only the server part

Package: stun-client
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Replaces: stun (<< 0.97~dfsg-1)
Breaks: stun (<< 0.97~dfsg-1)
Suggests: stun-server
Description: Test client for STUN
 The STUN protocol (Simple Traversal of UDP through NATs) is described in the
 IETF RFC 3489, available at http://www.ietf.org/rfc/rfc3489.txt.  It's used to
 help clients behind NAT to tunnel incoming calls through. This server is the
 counterpart to help the client identify the NAT and have it open the proper
 ports for it.
 .
 This package installs only the client.
